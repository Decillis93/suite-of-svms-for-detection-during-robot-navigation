import rospy
from sensor_msgs.msg import Image
import cv2
from cv_bridge import CvBridge
import numpy as np
import sklearn
from sklearn.externals import joblib
import scipy.ndimage
import scipy.misc
from sklearn.decomposition import PCA
from skimage import exposure
from skimage import feature
import imutils

# initializing pca
N_COMPS = 10
pca = PCA(n_components = N_COMPS)
pcae = PCA(n_components = N_COMPS)

# what image to analyze
SRC = "ABSOLUTE PATH TO YOUR IMAGE"

# importing svms model
svm_d = joblib.load("real-diag-handle-pyr.pkl")
svm_d1 = joblib.load("real-diag-handle-pyr-cascade.pkl")

svm_dsqrt = joblib.load("real-diag-handle-pyr-hog-sqrt.pkl")
svm_dsqrt1 = joblib.load("real-diag-handle-pyr-hog-sqrt-cascade.pkl")

# support vars
save = 1
windows = [[75,85],[45,51]]
# counter of the sub-images
cont = 0
# initialize list of boxes
rects = []

# Felzenszwalb et al.
def non_max_suppression_slow(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return []

    # initialize the list of picked indexes
    pick = []

    x1 = []
    y1 = []
    x2 = []
    y2 = []    
    area = []
	# grab the coordinates of the bounding boxes
    for i in range(len(boxes)):
        x1.append(boxes[i][0])
        y1.append(boxes[i][1])
        x2.append(boxes[i][2])
        y2.append(boxes[i][3])
        area.append((x2[i] - x1[i] + 1) * (y2[i] - y1[i] + 1))
 
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
		# grab the last index in the indexes list, add the index
		# value to the list of picked indexes, then initialize
		# the suppression list (i.e. indexes that will be deleted)
		# using the last index
		last = len(idxs) - 1
		i = idxs[last]
		pick.append(i)
		suppress = [last]
		# loop over all indexes in the indexes list
		for pos in xrange(0, last):
			# grab the current index
			j = idxs[pos]
 
			# find the largest (x, y) coordinates for the start of
			# the bounding box and the smallest (x, y) coordinates
			# for the end of the bounding box
			xx1 = max(x1[i], x1[j])
			yy1 = max(y1[i], y1[j])
			xx2 = min(x2[i], x2[j])
			yy2 = min(y2[i], y2[j])
 
			# compute the width and height of the bounding box
			w = max(0, xx2 - xx1 + 1)
			h = max(0, yy2 - yy1 + 1)
 
			# compute the ratio of overlap between the computed
			# bounding box and the bounding box in the area list
			overlap = float(w * h) / area[j]
 
			# if there is sufficient overlap, suppress the
			# current bounding box
			if overlap > overlapThresh:
				suppress.append(pos)
 
		# delete all indexes from the index list that are in the
		# suppression list
		idxs = np.delete(idxs, suppress)
 
	# return only the bounding boxes that were picked
    final = []
    for elem in pick:
        final.append(rects[elem])
    return final
	
if __name__ == "__main__":

    # select an image and open it in gray-scale
    gray = cv2.imread(SRC, 0)
    # create edge map
    edge = imutils.auto_canny(gray)
    # initialize score
    score_d = 0
    # for every window size
    for elem in windows:

        # defining support vars
        a = 0
        b = a + elem[0]
        c = 0
        d = c + elem[1]

        # sliding window cycle
        while (b <= len(gray)): # rows

            while (d <= len(gray[0])): # columns
                # create empty matrix
                img_cropped = np.zeros((elem[0],elem[1]),dtype=np.int)

                # populate the matrix
                for i in range(elem[0]):
                    img_cropped[i] = gray[i+a, c:d]                    
                
                pca.fit(img_cropped)
                
                # hog needs the same image size everytime, so we need to adjust different sized images
                if elem[0] != 75:
                    img_cropped = cv2.resize(img_cropped.astype("uint8"), (85,75))
                # if I want to save the sub-images, setting save to 1
                if save == 1:
                    cv2.imwrite("prova/my_image" + str(cont) + "-gray.png", img_cropped)
                    cont += 1
                
                # calculate sqrt of the image
                img_cropped = np.sqrt(img_cropped.astype("uint8"))
                # extract the histogram
                H = feature.hog(img_cropped, orientations=9, pixels_per_cell=(8, 8), cells_per_block=(2, 2), transform_sqrt=True)
                # predict with the couple of svms 
                res = svm_d.predict([pca.singular_values_])
                res += svm_dsqrt.predict([H])

                if res == 2:

                    res1 = svm_d1.predict([pca.singular_values_])
                    res1 += svm_dsqrt1.predict([H])

                    if res1 == 2:

                        # if all the layers have a positive outcome, add a box in the list
                        score_d += (res1/2)
                        rects.append([c,a,d,b])

                # updating vars
                c += 10
                d += 10

            a += 10
            b += 10
            c = 0
            d = c + elem[1]

    # delete redundant boxes                 
    rects = non_max_suppression_slow(rects, 0.1)
    # draw boxes on the image
    for elem in rects:
        cv2.rectangle(gray,(elem[0],elem[1]),(elem[2],elem[3]),(0,255,0),2)
    # show the final result
    cv2.imshow("final", gray)
    cv2.waitKey(0)