# README #

This repository contains part of the Master Thesis that I am developing for "La Sapienza" University of Rome. It consists of case detection applying Support
Vector Machine models, trained on dataset of images collected by myself during the experiments. When the agent encounters an unexpected situation, instead of
simply avoiding the obstacle or re-planning the entire path, a smart decision can be taken in according to the results of the classifiers.

I want to share my work in order to expand the databases and obtain feedbacks from other users. The objective is to create a set of classifiers that improves
the experience of navigation of an autonomous robot, offering an augmented perception of the surrounding environment through exploitation of sensors data.

Requirements:
Python 2.7 or 3.5, C++, ROS kinetic or higher, Gazebo or a robot compatible with ROS equipped with RGB and depth cameras (the classifiers work on every image, the compatibility with ROS is needed
to use the demo script)

There are already trained SVM model to recognize door handles, people and sensor malfunction due to the sun projection on the floor. Every 
classifier will be explained below, and it is supplied with the database on which it was trained and all the files relative to test and computation.

Gazebo:

Door handle detection: dataset composed of images of the "hinged door" handle. This set of classifiers is organized as a cascade, i.e. there are four
different layers, each one is formed by a couple of linear SVM model, one trained on normal gray-scale images reduced with PCA, the other is trained with 
histograms of oriented gradients extracted from preprocessed gray-scale or edge images. The overall decider possesses a good tested accuracy during 
navigation in the predefined maps of Gazebo. All the SVM models already trained are available.

Human detection: dataset composed of images depicting the model of a man in Gazebo. This set of classifiers is organized as a single couple, i.e. there is
only one layer, of linear SVM models, trained analogously to the previous situation.

Navigation system: Along with these two deciders, I uploaded also a navigation system that includes three ROS nodes.

- navigate.py is responsible for giving angular and linear velocity input to the agent in the simulation.

- gray-listener has the task of applying the above-mentioned classifiers

- depth-listener keeps the agent far from obstacles to avoid impacts and to acquire images

Physical Robot:

Door handle detection: datased composed of images depicting the door handles of the Computer Science department of "La Sapienza". It has two different layers
implemented similarly to the previuos.

Human detection: dataset built in according to the two RGB cameras of the Pepper robot from Aldebaran Robotics. One camera captures the face of the person,
the other captures the leg. In this way the classification is discriminating because to recognize an individual we need to identify legs and face.

Sun projected on the floor detection: many proximity sensors like ultrasounds or infrareds confuse the sun rays for an obstacle, effect that is visible also
from a depth image. Therefore, I create a decider that is able to distinguish the light of the sun projected on the floor from actual obstacles in the path.

Contact me at daniele93d@gmail.com for questions